# Concierge repository to manage the WAF rule repositories

A ModuleSync configuration manager with CI and automation in mind.

## WAF Module

The `configs/waf` directory hosts the configuration of the WAF rule repositories.

- `modulesync.yml`:
    Configuration for [ModuleSync](https://github.com/voxpupuli/modulesync), the repository synchronization tool.
- `managed_modules.yml`:
    List of repositories configured with the templates in the `moduleroot` subdirectory. Generated with the `concierge-cli`.
- `moduleroot/`:
    [ERB templates](https://puppet.com/docs/puppet/latest/lang_template_erb.html) for your repository configuration. Used by ModuleSync to align all repositories listed in `managed_modules.yml`. See [Puppet Labs configs](https://github.com/puppetlabs/modulesync_configs) for more details and explanation.

## Manage Setup

The [concierge-cli](https://pypi.org/project/concierge-cli/) helps you
automate the repository alignment process. 

Install it from PyPI:

```console
pip install concierge-cli
```

Update the `managed_modules.yml`:

1. Create a personal access token in GitLab with `api` scope: https://gitlab.com/profile/personal_access_tokens

2. Generate `managed_modules.yml` with `concierge-cli`
    ```console
    GITLAB_TOKEN=<TOKEN>

    concierge-cli gitlab --token ${GITLAB_TOKEN} --uri https://gitlab.com projects --topic waf > configs/waf/managed_modules.yml
    ```

## Configuration Details

See [configuration section](https://github.com/vshn/docker-concierge#configuration) of docker-concierge.

### When the server signature has changed

If SSH access to the GitLab server is refused, because the server signature changed:

1. Determine the new server signature (e.g. `ssh-keyscan gitlab.com`)
1. Update the value of the [`SSH_KNOWN_HOSTS`](https://gitlab.com/vshn/waf-demo/concierge/-/settings/ci_cd) variable accordingly
