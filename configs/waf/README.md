ModuleSync config: WAF rule repositories
========================================

Repository configuration files and management data.

Synced with [ModuleSync](https://github.com/voxpupuli/modulesync).
